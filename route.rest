GET http://localhost:5000/products?ids[]=7&ids[]=8

###

GET http://localhost:5000/products/001

###

POST http://localhost:5000/products
Content-Type: application/json

{
  "id": "001",
  "type": "food",
  "slug": "teriyaki-noodles",
  "image": "/images/teriyaki.jpg",
  "alt": "Teriyaki noodles",
  "name": "Teriyaki noodles",
  "price": 670,
  "description": "Spicy beef marinated in a teriyaki sauce, tossed with asian veggies, chillies, garlic, and a hint of lime.",
  "allergens": ["nuts", "sesame seeds", "eggs"]
}

###

PATCH http://localhost:5000/products/001
Content-Type: application/json
Accept: application/json

{
  "name": "Some other name"
}

###

DELETE http://localhost:5000/products/001
