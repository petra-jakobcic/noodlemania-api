// This file creates a model which will interact with the database.
const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  // Food & drinks
  id: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  // Food
  slug: {
    type: String
  },
  image: {
    type: String
  },
  alt: {
    type: String
  },
  description: {
    type: String
  },
  allergens: {
    type: Array
  },
  // Drinks
  category: {
    type: String
  }
});

module.exports = mongoose.model("Product", productSchema);
