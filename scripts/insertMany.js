// This file inserts the products from the 'products.json' file into our database.
require("dotenv").config();
const mongodb = require("mongodb");
const MongoClient = mongodb.MongoClient;
const products = require("../models/products.json");

const onConnect = (err, connection) => {
  if (err) throw err;

  const noodlemaniaDb = connection.db("noodlemania");

  const promises = products.map(product =>
    noodlemaniaDb.collection("products").insertOne(product)
  );

  // After inserting all the products into our database,
  // close the connection.
  Promise.all(promises)
    .then(() => console.log("Everything is inserted!"))
    .then(() => connection.close());
};

MongoClient.connect(process.env.DATABASE_URL, onConnect);
