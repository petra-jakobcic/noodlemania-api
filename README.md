# 🌍 Noodlemania-API

This is a RESTful API, which serves products (JSON) stored in a MongoDB database.

## ⚙️ Setting Up Your Development Environment

> IMPORTANT!
> You will need access to a MongoDB server.
> (Download here: https://www.mongodb.com/try/download/community)

1. Clone the project ([Bitbucket repository is here](https://bitbucket.org/petra-jakobcic/noodlemania-api/src)).
   ```
   git clone https://bitbucket.org/petra-jakobcic/noodlemania-api.git
   ```
2. Install the NPM dependencies.
   ```
   cd noodlemania-api
   npm install
   ```
3. Copy your `.env.example` and call it `.env`.
   ```
   cp .env.example .env
   ```
4. Edit the `.env` file to include a port number and the database DSN string.
5. Import the data into your database.
   ```
   node scripts/insertMany.js
   ```
6. Run the app in development mode.
   ```
   npm run devStart
   ```
