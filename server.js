// Require our dependencies.
require("dotenv").config();
const express = require("express");
const app = express();
const cors = require("cors");
const mongoose = require("mongoose");
const productsRouter = require("./routes/products");
const emailsRouter = require("./routes/emails");
const paymentRouter = require("./routes/payment");

// Connect to the database.
async function start() {
  try {
    await mongoose.connect(process.env.DATABASE_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });

    // Events on connection.
    const db = mongoose.connection;
    db.on("error", error => console.error(error));
    db.once("open", () => console.log("Now connected to the database..."));

    // Set up the server to accept JSON.
    app.use(express.json());

    // Serve static files with Express.
    app.use(express.static(__dirname + "/public"));

    // Cors
    app.use(
      cors({
        // origin: "http://localhost:3000"
        origin: "*" // applies to everything
      })
    );

    // Set up the routes.
    app.get("/", (req, res) => {
      res.sendFile(__dirname + "/index.html");
    });

    app.use("/products", productsRouter);
    app.use("/emails", emailsRouter);
    app.use("/payment", paymentRouter);

    app.listen(process.env.PORT, () => {
      console.log("Starting the server...");
      console.log(`Now listening on port ${process.env.PORT}...`);
    });
  } catch (err) {
    console.log("Oops, something went wrong...");
    console.log(err);
  }
}

start();
