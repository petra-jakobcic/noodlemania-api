const express = require("express");
const router = express.Router();
const Product = require("../models/product");

const productFields = [
  "id",
  "type",
  "name",
  "price",
  "slug",
  "image",
  "alt",
  "description",
  "allergens",
  "category"
];

// Get all products.
router.get("/", async (req, res) => {
  try {
    const products = await Product.find();
    setTimeout(() => res.json(products), 1500);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

// Get 1 product by its id.
router.get("/:id", getProductById, (req, res) => {
  res.json(res.product);
});

// Create a product.
router.post("/", async (req, res) => {
  const product = new Product({
    id: req.body.id,
    type: req.body.type,
    name: req.body.name,
    price: req.body.price,
    // Food
    slug: req.body.slug,
    image: req.body.image,
    alt: req.body.alt,
    description: req.body.description,
    allergens: req.body.allergens,
    // Drinks
    category: req.body.category
  });

  try {
    const newProduct = await product.save();
    res.status(201).json(newProduct); // status 201 = successfully created an object
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

// Update a product.
router.patch("/:id", getProductById, async (req, res) => {
  // Loop over the fields to update the ones found in the request body.
  productFields.forEach(field => {
    if (req.body[field] != null) {
      res.product[field] = req.body[field];
    }
  });

  try {
    const updatedProduct = await res.product.save();
    res.json(updatedProduct);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

// Delete a product.
router.delete("/:id", getProductById, async (req, res) => {
  const deletedProductId = res.product.id;

  try {
    await res.product.remove();
    res.json({ message: `Product ${deletedProductId} deleted!` });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

/**
 * Acts as middleware and gets a product by its ID.
 *
 * @param {Object} req The request object.
 * @param {Object} res The response object.
 * @param {Function} next A function which moves us to the next piece of
 *                        middleware or to the request itself.
 * @returns {void}
 */
async function getProductById(req, res, next) {
  let product;

  try {
    product = await Product.findOne({ id: req.params.id });

    if (product === null) {
      return res.status(404).json({ message: "This product does not exist." });
    }
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }

  // 'res.product' can be used in other functions and is set to the product defined in this middleware.
  res.product = product;

  next();
}

module.exports = router;
