const express = require("express");
const router = express.Router();

// Submit the Checkout form.
router.post("/", async (req, res) => {
  setTimeout(() => res.sendStatus(200), 1500);
});

module.exports = router;
